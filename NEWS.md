Summary of important user-visible changes for the upcoming release:
-------------------------------------------------------------------


Summary of important user-visible changes for version 0.0.4 (2024-11-12):
-------------------------------------------------------------------------

* New command `via.coco from-via3` to convert VIA3 project files into
  COCO data files.


Summary of important user-visible changes for version 0.0.3 (2024-08-13):
-------------------------------------------------------------------------

* Conversion of COCO result files is now supported.  Previously, only
  COCO data files were supported.

* The `pycocotools` Python package is now a dependency.


Summary of important user-visible changes for version 0.0.2 (2024-03-04):
-------------------------------------------------------------------------

* New `via.coco` module with command to convert COCO projects into
  LISA projects.


Summary of important user-visible changes for version 0.0.1 (2024-01-29):
-------------------------------------------------------------------------

* Initial release with `via.vfs` module to upload and download project
  files to VFS servers.
