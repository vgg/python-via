# Tools to handle VIA projects

[VIA](https://www.robots.ox.ac.uk/~vgg/software/via/) is a suite of
software tools developed by VGG to annotate images and videos.  This
package provides a Python interface to handle those annotations in
code, as well as a series of independent programs to handle them from
command line.

Some of the tools here can be found as independent scripts in the
[VIA](https://gitlab.com/vgg/via/),
[LISA](https://gitlab.com/vgg/lisa/), and
[VFS](https://gitlab.com/vgg/vfs/) git repositories.


## License

The python-via package is copyright of the University of Oxford and
licensed under the [Apache 2.0 License](./COPYING).
