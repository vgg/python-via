# Contributing

## Style

Sort imports with `isort` and style code with `black`.  The
`pyproject.toml` file at the root of the project already has the
configuration for those tools so you only need to:

    isort .  # sort imports
    black .  # apply code style

## Versioning

We use the `+dev` suffix to signal a development version on top of a
given release.  For example, version `1.0.2+dev` is the code for
release `1.0.2` with extra development on top which will eventually
become something "larger than" `1.0.2` such as `1.0.3`, `1.1.0`, or
`2.0.0`.

# Maintenance

## Release process

Releases are performed automatically by Gitlab when a `release-*` tag
is pushed (see `.gitlab-ci.yml`).  Releases are uploaded to PyPI.

These are the steps to be performed:

1. Specify the new version number, insert data into the NEWS file,
   commit, and tag it:

       NEW_VERSION="X.Y.Z"  # replace this with new version
       OLD_VERSION=`grep '^version ' pyproject.toml | sed 's,^version = "\([0-9.]*+dev\)"$,\1,'`
       python3 -c "from packaging.version import parse; assert parse('$NEW_VERSION') > parse('$OLD_VERSION');"

       sed -i 's,^version = "'$OLD_VERSION'"$,version = "'$NEW_VERSION'",' pyproject.toml
       ## Manually insert version number and date into NEWS.md file
       ## before commit.
       git commit -m "maint: release $NEW_VERSION" pyproject.toml NEWS.md
       COMMIT=$(git rev-parse HEAD | cut -c1-12)
       git tag -a -m "Added tag release-$NEW_VERSION for commit $COMMIT" release-$NEW_VERSION

2. Test that we can build a source and wheel distribution from a git
   archive export:

       rm -rf target
       git archive --format=tar --prefix="target/" release-$NEW_VERSION | tar -x
       (cd target/ ; python3 -m build)

   Performing a release from a git archive ensures that the release is
   not made with accidentally included modified or untracked files and
   is the closest to what Gitlab will actually release.

3. Add the `+dev` suffix back to the version string, a new line to
   NEWS file for the next version, commit, and push:

       sed -i 's,^version = "'$NEW_VERSION'"$,version = "'$NEW_VERSION'+dev",' pyproject.toml
       ## Manually insert a new section on the NEWS file for the next version.
       git commit -m "maint: set version to $NEW_VERSION+dev after $NEW_VERSION release." pyproject.toml NEWS.md
       git push upstream main
       git push upstream release-$NEW_VERSION

   After the push, Gitlab will notice the `release-*` which triggers a
   build of the package and an upload to PyPI.
